import os

#===============================================================================
# GENERAL 
#===============================================================================
HOME = os.path.dirname(os.path.realpath(__file__))+'/'
RESOURCES = HOME + 'resources/'
TMP = RESOURCES+'tmp/'
BACKEND = HOME + 'backend/'
FILES = HOME + 'files/'
TOOLS = HOME + 'tools/'
WEB_APP = HOME + 'webApp/'

#===============================================================================
# JOURNAL LIST
#===============================================================================
JOURNAL_LIST_URL = 'ftp://ftp.ncbi.nih.gov/pubmed/J_Medline.txt'
JOURNAL_LIST = RESOURCES+'J_Medline.txt'
JOURNAL_PICKLE = RESOURCES+'J_Medline.p'
JOURNAL_PICKLE_INVERTED = RESOURCES+'J_Medline_Inverted.p'
JOURNAL_CIVIC = RESOURCES+'civic_PublicationPerJournal.csv'

#===============================================================================
# MUTATIONS AND GENES 2 PUBMEDID 
#===============================================================================
PUBTATOR_INFO={        
            'GENES':{
                'url':'ftp://ftp.ncbi.nlm.nih.gov/pub/lu/PubTator/gene2pubtator.gz', 
                'downloadFile':RESOURCES+'genes_pubtator.gz', 
                'pickleFile':RESOURCES+'genes_pubtator.p',
                'gv':RESOURCES+'gv_genes.p',                
                },
          'MUTATIONS':{
                'url':'ftp://ftp.ncbi.nlm.nih.gov/pub/lu/PubTator/mutation2pubtator.gz', 
                'downloadFile':RESOURCES+'mutations_pubtator.gz', 
                'pickleFile':RESOURCES+'mutations_pubtator.p',
                'gv':RESOURCES+'gv_mutations.p',
                'nala':RESOURCES+'nala_mutations.p',
                }
           }

#===============================================================================
# PMC
#===============================================================================
PMC_DLPath = FILES+'pmc/'
fileList_PMC = RESOURCES+'oa_file_list.csv'
alreadyDownPMC = RESOURCES+'pmcList.p'

#===============================================================================
# PUBMED
#===============================================================================
PubMed_DLPath = FILES+'pubmed/'
alreadyDownPubMed = RESOURCES+'pubmedList.p'

#===============================================================================
# ASCO
#===============================================================================
ASCO_DL_Path = FILES +'asco/'
ASCO_DL_FILE = ASCO_DL_Path +'asco.jl'
ASCO_PREPARED_FILE = ASCO_DL_Path +'asco.p'
ASCO_ERROR_FILE=ASCO_DL_Path +'asco_error.csv'
alreadyDownASCO = RESOURCES +'ascoList.p'
inputFolder = ASCO_DL_Path+'gnpIn/'
outputFolder = ASCO_DL_Path+'gnpOut/'
tmpFolder = ASCO_DL_Path+'tmp/'

#===============================================================================
# NALA
#===============================================================================
nalaIn=FILES+'nala/in/'
nalaOut=FILES+'nala/out/'  
#===============================================================================
# ClinicalTrials.gov
#===============================================================================

#===============================================================================
# CLASSIFICATION STUFF
#===============================================================================
BEST = BACKEND+'best/'
BEST_IS = BEST+'isCancer_best.classifier'
NGRAM_IS = BEST+'isCancer_best.vectorizer'
BEST_WHICH = BEST+'which_best.classifier'
NGRAM_WHICH = BEST+'whichCancer.vectorizer'

#===============================================================================
# SOLR STUFF
#===============================================================================
CORE_NAME_WEBAPP='dci'
CORE_NAME='dci'
SOLR_LOCATION = '/home/solr-6.4.1'
SOLR = RESOURCES + 'solr/'
SOLR_TMP = SOLR+'tmp/'
SOLR_CONFIG = SOLR +'conf/'
SOLR_INDEX = SOLR+'data/'
SOLR_URL = 'http://localhost:8983/solr/{}'.format(CORE_NAME) 
SOLR_UPDATE = SOLR_URL+ '/update/json/docs'
SOLR_SELECT = SOLR_URL+ '/select'
SOLR_FIELDS =['nlm_unique_id', 'mesh_terms',  'pubdate',  'abstract',  'cancerType',  'pmc',  'mutation_normalizedValue', 'confidence_which', 'title',  'confidence_is',   'articleLink',  'pmid',  'journal',  'gene_name','source','authors'] 


#===============================================================================
# NET TOOLS
#===============================================================================
SETH = TOOLS+'SETH/'
SETH_JAR = SETH+ 'seth.jar'
TMVAR_JAR = TOOLS+'tmVarJava/tmVar.jar'
GNP = TOOLS+'GNormPlusJava/'
GNP_JAR = GNP+'GNormPlus.jar'

NEJI = TOOLS+ 'neji-2.0.2/'
NEJI_SH = NEJI + 'neji.sh'
NEJI_MODELS = NEJI + 'resources/models/'
NEJI_DICTIONARIES = RESOURCES+'nejiDicts'

#===============================================================================
# DB STUFF
#===============================================================================
DB_DUMP = WEB_APP+'createDB.sql'
DB_DUMP_EVAL = WEB_APP+'createDB_eval.sql'

#===============================================================================
# LOGGING
#===============================================================================
import logging
logging.basicConfig(filename=HOME+'debugg.log',level=logging.DEBUG)
logger_deubg=logging.getLogger(__name__)