import tornado.ioloop
import tornado.web
import tornado.httpserver
#from tornado.httpserver import HTTPServer
import requests
import MySQLdb as mdb
import traceback
import sys
import json
import ast
from operator import itemgetter
from urllib2 import *
from collections import Counter
import time
import operator
import cPickle as pickle
import pandas as pd

sys.path.insert(0, '../')
from _config import *

#
# EVAL STUFF
#

class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")

class MainHandlerEval(BaseHandler):
    def get(self):
        if not self.current_user:
            self.redirect("/login")
            return
        tornado.escape.xhtml_escape(self.current_user)
        # self.write("Hello, " + name)
        self.render("indexEval.html", title="Variant Information Search Tool (VIST) - Evaluation")

class QueryParametersEval:

    def __init__(self, nrDocuments, currentPage):

        # =======================================================================
        #     EXAMPLE
        #     mutation_normalizedValue:("V600E" OR "Val600Glu") AND gene_name:("MAP2K1" OR "BRAF") AND  pubdate:[2015 TO 2017]
        #    confidence_is desc, score desc, pubdate desc
        # =======================================================================

        self._headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)     Chrome/37.0.2049.0 Safari/537.36'
        }
        #self._reqPage = 'http://localhost:8984/solr/dci_eval/select'
        self._reqPage = SOLR_EVALUATION

        self._params = {
            'q.op': 'OR',
            "indent": "on",
            "sort": "product(query($q),confidence_is) desc",
            "wt": "json",
            "rows": nrDocuments,
            # 'start':int (data['nrDocuments'][0])* (int(data['currentPage'][0]) -1 ),
            'start': int(nrDocuments) * (int(currentPage) - 1),
            'q': '',
            'fl': '*, score'
        }

        self._journalList = pickle.load(open(JOURNAL_PICKLE, 'rb'))
        self._journalListInverted = pickle.load(open(JOURNAL_PICKLE_INVERTED, 'rb'))

    def prepareAnnotations(self, solrField, data):

        queryAnnotation = None
        if data:
            try:
                queryTerms = ['"' + item.strip() + '"' for item in data.split(',') if item]
                if len(queryTerms) > 1:
                    annotationList = ' OR '.join(queryTerms)
                elif len(queryTerms) == 1:
                    annotationList = queryTerms[0]
                queryAnnotation = '{}:({})'.format(solrField, annotationList)
            except Exception, e:
                print e
                print traceback.print_exc()

        # print solrField, queryAnnotation
        return queryAnnotation

    def executeQuery(self, query):

        res = None

        try:
            con = mdb.connect('localhost', USER, PASS, dbEval, use_unicode=True, charset='utf8');
            cur = con.cursor(mdb.cursors.DictCursor)
            cur.execute(query)
            con.commit()
            res = cur.lastrowid


        except Exception as e:
            traceback.print_exc()
            print e

        return res

    def filterCancerType(self, cancerType):

        cancerTypeFilter = None
        if cancerType != 'any' and cancerType != '' and cancerType != 'all':
            cancerTypeFilter = 'cancerType:"{}"'.format(cancerType)

        if cancerType == 'all':
            cancerTypeFilter = '-cancerType:"Not cancer"'

        return cancerTypeFilter

    def getYearsMinMax(self, queryObject, nrDocs):

        queryObject['fl'] = 'pubdate'
        queryObject['sort'] = 'pubdate desc'
        queryObject['rows'] = nrDocs
        queryObject['start'] = 0
        r = requests.get(self._reqPage, headers=self._headers, params=queryObject)
        response = r.json()
        candidates = [x['pubdate'] for x in response['response']['docs'] if len(str(x['pubdate'])) == 4]
        return (max(candidates), min(candidates))

    def getJournals(self, queryObject, nrDocs):

        # =======================================================================
        # REWRITE WITHOUT A NEW REQUEST TO SLOR BUT USING THE ORIGINAL RESULT SET
        # =======================================================================

        queryObject['fl'] = 'journal, source'
        queryObject['sort'] = ''
        queryObject['rows'] = nrDocs
        queryObject['start'] = 0
        r = requests.get(self._reqPage, headers=self._headers, params=queryObject)
        response = r.json()
        journals = []
        data = response['response']['docs']
        # print Counter([item['journal'] for item in response['response']['docs']])
        x = dict(Counter([item['journal'] for item in data]))
        # x = dict(sorted(x.items(), key=operator.itemgetter(1), reverse=True))
        # print x
        # raw_input('prompt')
        journals = [{'name': k, 'titleName': k + ' (' + str(v) + ')', 'countValue': v} for k, v in x.iteritems() if
                    k.strip() in self._journalList]
        journals = sorted(journals, key=itemgetter('countValue'), reverse=True)

        # =======================================================================
        # COUNT PER SOURCE -- CURRENTLY NOT USED
        # =======================================================================
        sourceCount = {
            'all': 0,
            "pubmed": 0,
            "ASCO": 0,
            "pmc": 0
        }

        source = [item['source'] for item in data]
        sourceCountTmp = dict(Counter(source))
        sourceCountTmp['all'] = len(source)
        sourceCount.update(sourceCountTmp)
        sourceCount = dict(sorted(sourceCount.items(), key=operator.itemgetter(1), reverse=True))
        sourceCount = [{'source': k, 'occurences': v} for k, v in sourceCount.iteritems()]
        return (journals, sourceCount)

    def alreadyEvaluated(self, userId, queryId):

        print userId, queryId
        resList = None
        query = """SELECT feedback_relevant as useful,  
                                    feedback_correctClassification as classification, 
                                    feedback_queryId as queryId,
                                    feedback_pmid as pmid
                        FROM feedback_evaluation 
                        where feedback_evaluation.feedback_queryId='{}' and feedback_user='{}'""".format(queryId,
                                                                                                         userId)

        try:
            con = mdb.connect('localhost', USER, PASS, dbEval, use_unicode=True, charset='utf8');
            cur = con.cursor(mdb.cursors.DictCursor)
            cur.execute(query)
            dummyVar = cur.fetchall()

            if len(dummyVar) > 0:
                resList = dummyVar

        except mdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            print query
            traceback.print_exc()

        # print resList
        return resList
        # print json.dumps(resList)
        # return json.dumps(resList)

class QueryHandlerEval(BaseHandler):

    @tornado.web.authenticated
    def get(self):

        start = time.time()
        start_all = time.time()
        resList = {
            'docs': [],
            'numFound': 0,
            'journals': [],
            'queryID': [],
            'minPublication': 0,
            'maxPublication': 0
        }

        genes = None
        mutations = None
        response = {}
        # userID = 0

        # ===================================================================
        # SOLR QUERY
        # ===================================================================
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)     Chrome/37.0.2049.0 Safari/537.36'
        }
        reqPage = SOLR_EVALUATION

        # =======================================================================
        # REQUEST DATA
        # =======================================================================
        data = self.request.arguments
        params = {
            'group': 'true',
            'group.field': 'pmid',
            'group.main': 'true',
            'q.op': 'OR',
            "indent": "on",
            "sort": "product(query($q),confidence_is) desc",
            "wt": "json",
            'rows': 10,
            'q': '',
            'fl': '*, score'
        }

        userID = tornado.escape.xhtml_escape(self.current_user)
        queryID = data['queryID'][0]

        # =======================================================================
        # QUERY PARAMS
        # =======================================================================
        # print 'Recieved Data:\t', data
        qParams = []
        pqa = QueryParametersEval(data['nrDocuments'][0], data['currentPage'][0])
        print 'QueryParameters\t', (time.time() - start)
        start = time.time()

        try:
            genes = pqa.prepareAnnotations('gene_name', data['genes'][0])
            if genes is not None:
                qParams.append(genes)
            print 'genes\t', (time.time() - start)
            start = time.time()
        except KeyError:
            pass

        try:
            mutations = pqa.prepareAnnotations('mutation_normalizedValue', data['mutations'][0])
            if mutations is not None:
                qParams.append(mutations)
            print 'mutations\t', (time.time() - start)
            start = time.time()
        except KeyError:
            pass

            # FILTER YEARS IF POSSIBLE
        # if data['minFiltered'][0] and data['maxFiltered'][0]:
        try:
            qParams.append('pubdate:[{} TO {}]'.format(data['minFiltered'][0], data['maxFiltered'][0]))
        except KeyError, e:
            # traceback.print_exc()
            pass
            # pas

        # FILTER SOURCE IF POSSIBLE
        try:
            if data['source'][0] != 'all':
                data['source'][0]
                qParams.append('source:{}'.format(data['source'][0]))
        except KeyError:
            pass

        # cancer types
        # if pqa.filterCancerType(data['cancerType'][0]):
        try:
            cType = pqa.filterCancerType(data['cancerType'][0])
            if cType is not None:
                qParams.append(cType)
        except KeyError:
            pass

        # FILTER JOURNALS IF POSSIBLE
        if 'journals' in data:
            if len(data['journals']) == 1:
                '"' + data['journals'][0] + '"'
                # params["fq"] = 'journal={}'.format(journalFilter)
            else:
                ' OR '.join(['"' + item + '"' for item in data['journals']])
            # qParams.append('journal:({})'.format(journalFilter))

        # PUTING THE PARAMS TOEGETHER
        if len(qParams) > 1:
            params["q"] = ' AND '.join(qParams)
        else:
            params["q"] = ''.join(qParams)

        # =======================================================================
        # if len(qParams)>1:
        #     params["fq"] = ' AND '.join(fqparams)
        # else:
        #     params["q"] = ''.join(fqparams)
        # =======================================================================

        print 'apply filters\t', (time.time() - start)
        start = time.time()

        try:

            # r = requests.get(reqPage, headers=headers, params=params)
            print reqPage
            r = requests.post(reqPage, data=params)
            # print r.url
            # print r.text
            response = r.json()
            print 'Retrieving documents from Solr:\t', (time.time() - start)
            start = time.time()
            alredayEvaluated = pqa.alreadyEvaluated(userID, queryID)
            print 'alredayEvaluated', alredayEvaluated
            print 'DB stuff\t', (time.time() - start)
            start = time.time()

            if response['response']['numFound'] > 0:
                resList = {
                    'docs': response['response']['docs'],
                    'alreadyEvaluated': alredayEvaluated
                }

                for k, v in resList.iteritems():
                    if k not in ['docs', 'journals']:
                        print k, v, type(v)
                    else:
                        print k, len(v)

            print 'process response\t', (time.time() - start)
            print 'Duration all \t ', (time.time() - start_all)

        except Exception as e:
            print e
            traceback.print_exc()

        print '************************************************'
        self.write(json.dumps(resList))

class CivicHandlerEval(BaseHandler):
    # s@tornado.web.authenticated

    def get(self):
        self.request.arguments
        civic = pd.read_csv(JOURNAL_CIVIC)
        journals = [{'name': x[0], 'titleName': x[0] + ' (' + str(x[1]) + ')', 'countValue': x[1]} for x in
                    civic.itertuples(False)]
        self.write(json.dumps(journals))

class FeedbackHandlerEval(MainHandlerEval):

    def storeFinishedQuery(self, userId, queryId):

        resList = {'finishedStored': False}

        try:
            con = mdb.connect('localhost', USER, PASS, dbEval, use_unicode=True, charset='utf8')
            cur = con.cursor(mdb.cursors.DictCursor)
            sql = "INSERT INTO finished VALUES ({},{})".format(queryId, userId)
            cur.execute(sql)
            con.commit()
            con.close()
            resList = {'finishedStored': True}

        except Exception as e:
            print e
            traceback.print_exc()

        return resList

    def storeFeedback(self, feedbackItems):

        resList = None
        print 'storeFeedback\t', feedbackItems

        try:

            for item in feedbackItems:
                con = mdb.connect('localhost', USER, PASS, dbEval, use_unicode=True, charset='utf8')
                cur = con.cursor(mdb.cursors.DictCursor)

                query = """INSERT INTO feedback_evaluation 
                                    (feedback_user,feedback_relevant,feedback_correctClassification,feedback_queryId,feedback_pmid) 
                                    VALUES ({},'{}','{}',{},'{}')
                                    ON DUPLICATE KEY UPDATE
                                    feedback_relevant='{}',
                                    feedback_correctClassification='{}'; 
                                    """.format(int(item[0]), item[1], item[2], int(item[3]), item[4], item[1], item[2])

                # print query
                cur.execute(query)
                con.commit()
                con.close()

            resList = {'feedbackStored': True}

        except Exception as e:
            print e
            traceback.print_exc()

        return resList

    # @tornado.web.authenticated
    def get(self):
        resList = []
        feedbackItems = []
        print 'current user\t' + tornado.escape.xhtml_escape(self.current_user)
        try:
            userId = tornado.escape.xhtml_escape(self.current_user)
            data = self.request.arguments
            res = [ast.literal_eval(item) for item in data['feedbackData']]
            # ===================================================================
            # print data
            # print res
            # print data['feedbackType']
            # ===================================================================

            for item in res:
                feedbackItems.append((userId, item["useful"], item['classification'], item['queryId'], item["pmid"]))

            queryId = res[0]['queryId']

            resList.append(self.storeFeedback(feedbackItems))

            if data['feedbackType'][0] == '0':
                resList.append(self.storeFinishedQuery(userId, queryId))

        except Exception:
            traceback.print_exc()

        self.write(json.dumps(resList))

class evaluationQueriesHandler(BaseHandler):
    def get(self):
        resList = {}
        try:
            con = mdb.connect('localhost', USER, PASS, dbEval, use_unicode=True, charset='utf8')
            cur = con.cursor(mdb.cursors.DictCursor)

            userID = tornado.escape.xhtml_escape(self.current_user)
            # print userID
            cur.execute(
                "select * from evaluationQueries where evaluationQueries_id not in (select queries_queries_id FROM finished where user_iduser='{}')".format(
                    userID))
            resList = cur.fetchall()
            con.commit()
            con.close()

        except Exception as e:
            print e

        # print json.dumps(resList)
        self.write(json.dumps(resList))