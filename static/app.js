//var history_api = typeof history.pushState !== 'undefined';
//if ( history_api ) history.pushState(null, '', '#StayHere');

// Define the `phonecatApp` module
var app = angular.module(
		'classification',
		['ngRoute','ngAnimate','ngMaterial','ngAria','ngMessages', 'angAccordion',
		'rzModule','angular-loading-bar','angucomplete-alt', 'ngMdIcons',
        'md.data.table', 'angularUtils.directives.dirPagination', 'vAccordion',
        'ngDialog','ngSanitize']
);


app.config(function ($interpolateProvider) {$interpolateProvider.startSymbol('[[').endSymbol(']]')});

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

app.config(function($mdThemingProvider){
    // Update the theme colors to use themes on font-icons
    $mdThemingProvider.theme('default')
          .primaryPalette("red")
          .accentPalette('green')
          .warnPalette('blue');
  	    //.backgroundPalette('white');

    $mdThemingProvider.theme('custom')
	    .primaryPalette('grey')
	    .accentPalette('deep-purple')
	    .warnPalette('green');

	    $mdThemingProvider.theme('forest')
	    .primaryPalette('brown')
	    .accentPalette('green')
        .warnPalette('cyan')
	    .backgroundPalette('light-blue');
  });

app.config(function($mdIconProvider) {
    $mdIconProvider
    .iconSet('social', 'img/icons/sets/social-icons.svg', 24)
    .defaultIconSet('img/icons/sets/core-icons.svg', 24);
});


app.controller('lala', ['$scope', '$sce', '$log', '$http', '$timeout', '$mdSidenav','$window',
				function($scope, $sce, $log, $http, $timeout, $mdSidenav,$window) {

	//DISABLE BACK BUTTON
//	$scope.$on('$locationChangeStart', function(event, next, current){
//	    // Here you can take the control and call your own functions:
//	    //alert('Sorry ! Back Button is disabled');
//	    // Prevent the browser default action (Going back):
//	    event.preventDefault();
//	});

	/*
	 * INITIALIZE EVERYTHING NEEDED TO
	 */
	$scope.init = function() {
			//$scope.resetValues();
			$scope.autoSaveFeedback = 10;
			$scope.lastQueryID=null;
			$scope.currentPage=1;
			//$scope.userId = userId;
            $scope.exampleQueries();
			console.log('Everything set up!');
			$scope.message = '';
			$scope.highlight = "Show";
		};

		$scope.resetValues = function() {

			$scope.journals = [];
			$scope.selectedJournals = [];
			$scope.responseData = [];
			$scope.CTresponseData = [];
			// $scope.responseDataContainer = [];
			$scope.currentPage=1;

	  		delete $scope.queryTerms['minYear'];
	  		delete $scope.queryTerms['maxYear'];
	  		delete $scope.queryTerms['minFiltered'];
	  		delete $scope.queryTerms['maxFiltered'];
	  		$scope.queryTerms['currentPage']=1;
	  		delete $scope.queryTerms['queryID'];
	  		delete $scope.queryTerms['journals'];
	  		delete $scope.queryTerms['sourceCount'];
	  		delete $scope.queryTerms['cancerType'];
	  		delete $scope.queryTerms['civicJournals'];
	  		delete $scope.queryTerms['source'];
	  		// delete $scope.queryTerms['keywords'];

			/*
			 * SHOW/DISABLE FLAGS
			 */
			$scope.filterDisable = true;
			$scope.isShown = true;
			$scope.isShownFeedback = false;
			$scope.selectedTab=0;
			 $scope.civicJournals = null;
			 $scope.chooseCivic = false;
			 $scope.queryError=false;
			 $scope.noResults=false;

		};

		/*
		*	APP VARIABLES
		*/
		$scope.journals = [];
		$scope.journalsTmp = []
		$scope.selectedJournals = [];
		$scope.selectedJournal = null;
		$scope.feedbackData = [];
		$scope.classificationData = [];
		$scope.responseData = [];
		$scope.CTresponseData = [];
		// $scope.responseDataContainer = [];
		$scope.lastQueryID=-1;
		$scope.currentPage=1;
		 $scope.isShown = true;
		 $scope.isShownFeedback = false;
		 $scope.sourceCount = null;
		 $scope.selectedTab=0;
		 $scope.civicJournals = false;
		 $scope.chooseCivic = false;
		 $scope.queryError=false;
		 $scope.noResults=false;
		/*
		 * CANCER TYPE OBJECT
		 */
		$scope.cancerType = [
		  	{ id: -1, disease:'any', name: 'All documents' },
		  	{ id: 0, disease:'all', name: 'All cancer types' },
		    { id: 1, disease:'colorectal cancer', name: 'ColoRectal' },
		    { id: 2, disease:'head and neck cancer', name: 'Head&Neck' },
		    { id: 3, disease:'melanoma', name: 'Melanoma' },
		    { id: 4, disease:'disease',name: 'Other cancer' },
		    { id: -2, disease:'Not cancer', name: 'Non-cancer documents' }
		  ];
		$scope.selectedCancer = { id: -1, disease:'any', name: 'All documents' };

		/*
		 * SLIDER
		 */
		var currentTime = new Date();
		$scope.year = currentTime.getFullYear();
		$scope.minValue = null;
		$scope.maxValue = $scope.year;
		$scope.minFiltered = null;
		$scope.maxFiltered = null;


//		TOP N CIVIC JOURNALS
		$scope.civicSize = [5,10,20,'All']

		/*
		 * QUERY OBJECT
		 */
		$scope.queryTerms={
				'genes':'',
				'mutations':'',
				'keywords':'',
				'minYear':$scope.minValue ,
				'maxYear':$scope.maxValue,
				'currentPage':$scope.currentPage,
				'nrDocuments':100,
				'cancerType':'',
				'queryID':$scope.lastQueryID,
				//'userID':$scope.userId,
				'journals':$scope.selectedJournals,
				};


    /*
     * DUMMY SEARCH OBJECT
     */

	$scope.searchFunction = function (queryObject) {

	    $scope.responseData = [];
	    $scope.CTresponseData = [];
	    console.log(queryObject);

		  $http({
		         method: 'GET',
		         url: '/getQuery',
		         params: queryObject,
		         headers: {
				        'Content-Type': 'application/json'
				    }
		     }).then(function(response) {
		    	 $scope.status = response.status;
		          $scope.responseData = angular.fromJson(response.data['docs']);
				  $scope.CTresponseData = angular.fromJson(response.data['ct']);
		          // $scope.responseDataContainer = response.data['docs'];
		          $scope.numFound= response.data['numFound'];
		          $scope.numFoundCT= response.data['numFoundCT'];
		          // $scope.pagination=Math.round((parseInt($scope.numFound)/parseInt($scope.queryTerms['nrDocuments']))+1);
		          $scope.pagination=parseInt((parseInt($scope.numFound)/parseInt($scope.queryTerms['nrDocuments']))+1);
		          $scope.journals =  response.data['journals'];
		          queryObject['queryID']=response.data['queryID'];
		          $scope.queryTerms['queryID']=response.data['queryID'];
		          $scope.lastQueryID=response.data['queryID'];
		  		  $scope.minValue = response.data['minPublication'];
				  $scope.maxValue = response.data['maxPublication'];
				  $scope.minFiltered = response.data['minPublicationFilter'];
				  $scope.maxFiltered = response.data['maxPublicationFilter'];
				  $scope.sourceCount=response.data['sourceCount'];
				  $scope.currentNavItem = 'medline';
				  // console.log(response.data);
				  console.log($scope.sourceCount);
				  // console.log(response.data);
					$scope.slider = {
						    minValue: $scope.minFiltered,
						    maxValue: $scope.maxFiltered,

						    options: {
						       floor: $scope.minValue,
						       ceil: $scope.maxValue,
						        id: 'slider-id',
								step: 1,
    							showTicks: false
						    }
						};

						$scope.normalizeSeectedQueryJournals();

					if ($scope.numFound == 0){
						$scope.noResults=true;
						$scope.message = 'The query found no matching documents. Please refine your query or start over.';
					} else {
					    $scope.message = ''
                    }


		     }, function(response) {
		          $scope.status = response.status;
		          $scope.responseData = [];
		          $scope.queryError;
		          $scope.message = 'There was an error with the query. Please start over.';
		          alert('There was an error in the query');
		    });
  		};

  		//AJAX TOP CIVIC
  		$scope.getTopCivic = function () {
  		  $http({
  		         method: 'GET',
  		         url: '/getCivic',
  		         params: {'TopCivic':$scope.civicJournals},
  		         headers: {
  				        'Content-Type': 'application/json'
  				    }
  		     }).then(function(response) {
  		    	 console.log(response.data);
  		    	//responseData = response.data['docs'];
  		    	$scope.selectedJournals=response.data;
  		     }, function(response) {
  		          $scope.status = response.status;
  		    });
    		};

    //INITIAL SEARCH
  	$scope.search = function() {

	    $scope.message = 'Please, wait for the query to execute.';
  		$scope.selectedCancer = { id: -1, disease:'any', name: 'All documents' };

    	if  ($scope.feedbackData.length >  0 ){
			$scope.feedback();
    	};

  		$scope.resetValues();
        console.log('this is it, search is int');
  		$scope.searchFunction($scope.queryTerms);
	}

	  	/*
	  	 * FILTER STUFF
	  	 */

	//	PAGING FUNCTION
		$scope.getPage = function () {

			if ($scope.currentPage <= $scope.pagination){
                $scope.message = 'Fetching results page '+$scope.currentPage +'. Please, wait for the query to execute.';
		  		$scope.queryTerms['minYear']=$scope.slider['options']['floor'];
		  		$scope.queryTerms['maxYear']=$scope.slider['options']['ceil'];
				$scope.queryTerms['minFiltered'] = $scope.slider['minValue'];
				$scope.queryTerms['maxFiltered'] = $scope.slider['maxValue'];
		  		$scope.queryTerms['journals']=$scope.selectedJournals.map(function(a) {return a.name;});
		  		$scope.queryTerms['cancerType']=$scope.selectedCancer.disease;
		  		$scope.queryTerms['sourceCount']=$scope.sourceCount;
		  		$scope.queryTerms['currentPage']=$scope.currentPage;
			    $scope.searchFunction($scope.queryTerms);
			    } else {
				$scope.currentPage = parseInt((parseInt($scope.numFound)/parseInt($scope.queryTerms['nrDocuments']))+1);
				alert('Maximum number of pages exceeded. Please revise your query');

			}

		  	};

		 //FILTER FUNCTION
	  	$scope.filter = function(){
	  		//console.log('journalsTMP'+$scope.journalsTmp);

			if ('source' in $scope.queryTerms) {
			    	delete $scope.queryTerms['source'];
				}

            $scope.message = 'Applying selected filters. Please, wait for the query to execute.';
	  		$scope.queryTerms['minYear']=$scope.slider['options']['floor'];
	  		$scope.queryTerms['maxYear']=$scope.slider['options']['ceil'];
			$scope.queryTerms['minFiltered'] = $scope.slider['minValue'];
			$scope.queryTerms['maxFiltered'] = $scope.slider['maxValue'];
	  		$scope.queryTerms['currentPage']=1;
	  		$scope.queryTerms['journals']=$scope.selectedJournals.map(function(a) {return a.name;});
	  		$scope.queryTerms['cancerType']=$scope.selectedCancer.disease;
	  		$scope.queryTerms['sourceCount']=$scope.sourceCount;
//	  		$scope.resetValues();
	  		$scope.searchFunction($scope.queryTerms);
	  	};

		 //SOURCE FILTER FUNCTION
  		$scope.filterSource = function(source, tabIndex){

  				//console.log($scope.lastQueryID);

				if ($scope.lastQueryID){

			  			if (source !=  'all') {
			  				$scope.queryTerms['source']=source;
			  			} else {
			  				if ('source' in $scope.queryTerms) {
			  				    	delete $scope.queryTerms['source'];
			  					}
			  				}

			  		$scope.selectedTab=tabIndex;
			  		$scope.queryTerms['minYear']=$scope.slider['options']['floor'];
			  		$scope.queryTerms['maxYear']=$scope.slider['options']['ceil'];
					$scope.queryTerms['minFiltered'] = $scope.slider['minValue'];
					$scope.queryTerms['maxFiltered'] = $scope.slider['maxValue'];
			  		$scope.queryTerms['currentPage']=1;
			  		$scope.queryTerms['journals']=$scope.selectedJournals.map(function(a) {return a.name;});
			  		$scope.queryTerms['cancerType']=$scope.selectedCancer.disease;
			  		//console.log($scope.queryTerms);
			  		$scope.searchFunction($scope.queryTerms);
			  		$scope.queryTerms['sourceCount']=$scope.sourceCount;
				}
		};

/*		$scope.filterCancerTypeServer = function(){
	      $scope.lastQuery['cancerType']=$scope.selectedCancer.disease;
	      $scope.searchFunction($scope.queryTerms);
  		};	  	*/

  		$scope.normalizeSeectedQueryJournals = function() {
		  	  if ($scope.selectedJournals.length > 0){
		      	  //console.log($scope.selectedJournals.length);
		  		  $scope.selectedJournals.forEach( function (arrayItem1, index1, object1) {
		  			  var selectedExists = false;
		          	  $scope.journals.forEach( function (arrayItem, index, object) {
		          		  //console.log(arrayItem.name + '\t\t' + arrayItem1);
		              	  	if ( arrayItem1.name == arrayItem.name) {
		      		    	        //console.log('normalizeSeectedQueryJournals'+'\t\t'+arrayItem['name'] + '\t\t' + arrayItem1);
		      		    	        selectedExists = true;
		      		    	        object.splice(index, 1);
	      		    	    		}
	              	  		})// inner journals for each

                		if (selectedExists === false){
                			 $scope.selectedJournals.splice(index1, 1);
                		}
	  		  		})
	  	  		}
  		};

 /*
 * FEEDBACK FOR RESULTS - ADD AND REMOVE ITEMS FROM FEEDBACK LIST
 */
    $scope.addRelevance = function(pmid, value, qId, type) {

    	console.log(pmid, value, qId, type);

    	var notFound = true;
        for(var i=0; i < $scope.feedbackData.length; i++) {
            if($scope.feedbackData[i].pmid == pmid) {
            	if (type == 'relevant'){
            		$scope.feedbackData[i].useful=value;
            	} else {
            		$scope.feedbackData[i].classification=value;
            	};
            	notFound = false;
                break;
            }
        }

        if(notFound){

        	if (type == 'relevant'){
        		$scope.feedbackData.push({'pmid':pmid,'useful':value,'classification':'','queryId':qId});
        	} else {
        		$scope.feedbackData.push({'pmid':pmid,'useful':'','classification':value,'queryId':qId});
        	};


        }
        console.log($scope.feedbackData);
        $scope.selectedOption = null;
        $scope.selectedOptionCorrect = null;
    };

    $scope.removeItem = function(pmid) {
    	for(var i=0; i < $scope.feedbackData.length; i++) {
            if($scope.feedbackData[i].pmid == pmid) {
            	$scope.feedbackData.splice(i, 1);
                break;
            }
        }
    };

    $scope.$watchCollection('feedbackData', function() {
    	if  ($scope.feedbackData.length == $scope.autoSaveFeedback ){
    			$scope.feedback();
    	};

    	if  ($scope.feedbackData.length == 0 ){
    		$scope.isShownFeedback = false;
	};

    });

    $scope.feedback = function () {

      $scope.feedbackResponse = null;
      $scope.feedbackStatus = null;

  	  $http({
  	         method: 'GET',
  	         url: '/feedback',
  	         params:$scope.feedbackData,
  	         headers: {
  			        'Content-Type': 'application/json'
  			    }
  	     }).then(function(response) {
  	          $scope.feedbackResponse = response.data;
  	     }, function(response) {
  	          $scope.feedbackResponse = response.data || 'Request failed';
  	    });
  	  $scope.feedbackData = [];

    };

    /*
     * REMOVE FROM JOURNAL SELECTION
     */
    $scope.deleteJournal = function(journal){
    	//console.log($scope.journals);
    	//console.log('Delete journal ' + journal);
    	$scope.selectedJournals.forEach( function (arrayItem, index, object) {
    		if (arrayItem['name'] === journal) {
    			console.log(object);
    			console.log(arrayItem);
    			$scope.journals.push(arrayItem);
    			object.splice(index, 1);
    			$scope.journals = _.sortBy($scope.journals, 'countValue').reverse();
    			return true;
    			}
    		})
    };

      /*
       * SELECTED JOURNALS
       */
      $scope.$watchCollection('selectedJournal', function() {

    	  if ($scope.selectedJournal != null){
    		  //console.log('Journals:\t'+$scope.journals);
    		  console.log('Selected Journal:\t'+$scope.selectedJournal);
        	  $scope.selectedJournals.push($scope.selectedJournal.originalObject);
        	  console.table($scope.selectedJournals);
        	  $scope.journals.forEach( function (arrayItem, index, object) {
        		  	//console.log(arrayItem['name']);
            	  	if (arrayItem['name'] === $scope.selectedJournal.originalObject.name) {
    		    	        object.splice(index, 1);
    		    	    }
              		})
    	  		}
        	});

      $scope.$watchCollection('chooseCivic', function() {

    	  if (!$scope.chooseCivic){
        	  	$scope.selectedJournals=[];
        	  	//$scope.journals = $scope.originalJournals;
    	  		}
        	});

  	/*
  	 * LOGOUT
  	 */
  	$scope.logout = function(){
  		if  ($scope.feedbackData.length > 0){
  			$scope.feedback();
  		};
  		$window.location.href = '/logout';
  	};

  	$scope.switchCollapse = function(){
  			$scope.isShown = !$scope.isShown;
  	};

  	$scope.toggleViewFeedback = function(){
			$scope.isShownFeedback = !$scope.isShownFeedback;
	};

	/*
	 * SIDE NAV
	 */
    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
    $scope.toggleExamples = buildToggler('examples');

    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      };
    };


	/*
	 * SIDE NAV
	 */

	$scope.exampleQueries = function () {
  		  $http({
  		         method: 'GET',
  		         url: '/getQueriesExamples',
  		         headers: {
  				        'Content-Type': 'application/json'
  				    }
  		     }).then(function(response) {
  		    	 console.log(response.data);
  		    	$scope.qExamples=response.data;
  		     }, function(response) {
  		          $scope.status = response.status;
  		    });
    		};

    $scope.selectQueryExample = function (param1) {
        $scope.responseData=[];
        $scope.queryTerms.genes=$scope.qExamples[param1].evaluationQueries_genes;
        $scope.queryTerms.mutations=$scope.qExamples[param1].evaluationQueries_mutations;
    };

    /*
    * IMAGES STUFF
     */
    $scope.myImages = ["exampleQueriesAnnotate.png", "resultsAnnotatedMain.png", "searchAnnotated.png"];
	$scope.getImagePath = function(imageName) {
		return "https://localhost/img/" + imageName;
	};

	/*
	ACCORDION FOR HIGHLIGHTING
	 */

	//https://www.thepolyglotdeveloper.com/2015/05/highlight-text-in-a-string-using-javascript-and-angularjs/

	$scope.regEscape = function(string){
		return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
	};

	$scope.highlightAnnotations = function(haystack, needle, cssClass) {
		// console.log(haystack, needle, cssClass);
        if(!needle) {
			return $sce.trustAsHtml(haystack);
			}

    	return $sce.trustAsHtml(haystack.replace(new RegExp($scope.regEscape(needle.split('|')[0]), "gi"), function(match) {
        	return '<span style="background-color:'+cssClass+'">' + match + '</span>';
    	}));
	};

	$scope.expandCallbackAlt = function (index, id){
		// console.log($scope.responseData[index]);
		// console.log(angular.fromJson($scope.responseData[index]));
		$scope.responseData[index].abstract_highlight=$scope.responseData[index].abstract;
		$scope.$watch('highlights', function () {
            var offset = 0;
            positionArray = Array();
            $scope.responseData[index].abstract_highlight = $scope.responseData[index].abstract;
			$scope.highlights.forEach( function (arrayItem){
				if(arrayItem.value === 'Show'){
					if($scope.responseData[index].hasOwnProperty(arrayItem.modelField)){
						// console.log(arrayItem.entitiesField, $scope.responseData[index].abstract_highlight);
						if(arrayItem.label === 'Sentences') { //HIGHLIGHT SENTENCES
                            for (var i = $scope.responseData[index][arrayItem.modelField].length; i > 0; i--) {
                                var positions = JSON.parse($scope.responseData[index][arrayItem.modelField][i - 1]);
                                startText = $scope.responseData[index].abstract_highlight.substring(0, positions[0]);
                                slicedText = $scope.responseData[index].abstract_highlight.substring(positions[0], positions[1]);
                                endText = $scope.responseData[index].abstract_highlight.substring(positions[1]);
                                $scope.responseData[index].abstract_highlight = startText + "<span style=\"background-color: rgba(255,255,0," + positions[2] + ")\">" + slicedText + "</span>" + endText;
                            }
                            $scope.responseData[index].abstract_highlight=$sce.trustAsHtml($scope.responseData[index].abstract_highlight);
							// console.log('Sentences', $sce.valueOf($scope.responseData[index].abstract_highlight));
                        } else { // HIGHLIHGT OTHERS
							// console.log(arrayItem.entitiesField, $scope.responseData[index].abstract_highlight);
							// console.log($scope.responseData[index][arrayItem.entitiesField]);
							$scope.responseData[index][arrayItem.entitiesField].map( function(annItem) {
								// console.log(arrayItem.entitiesField, $sce.valueOf($scope.responseData[index].abstract_highlight));
								// console.log(annItem, typeof annItem);
								// console.log(annItem.split(','));
								// console.log(angular.fromJson(annItem));
								// console.log(angular.toJson(annItem));
								$scope.responseData[index].abstract_highlight= $scope.highlightAnnotations($sce.valueOf($scope.responseData[index].abstract_highlight),annItem,arrayItem.css);
							});
						}
					} // HIGHLIHGT SENTENCES
				}
			});

		}, true);
	};

	$scope.expandCallbackCT = function (index, id){
		// alert('Here we are');
		// console.log($scope.CTresponseData[index]);
		// console.log(angular.fromJson($scope.CTresponseData[index]));
		console.log($scope.CTresponseData[index].abstract_highlight);
		$scope.$watch('highlights', function () {
            var offset = 0;
			$scope.CTresponseData[index].abstract_highlight='<b>SUMMARY</b>\t'+$scope.CTresponseData[index].brief_summary + '<br/><br/><b>DESCRIPTION\t</b>' +  $scope.CTresponseData[index].description + '<br/><br/><b>INTERVENTION</b>\t' + $scope.CTresponseData[index].intervention_description;
			$scope.CTresponseData[index].abstract_highlight=$scope.CTresponseData[index].abstract_highlight.replace(/\\n/g, '<br />');
			$scope.CTresponseData[index].abstract_highlight=$scope.CTresponseData[index].abstract_highlight.replace(/\n/g, '').replace(/  +/g, ' ');
            positionArray = Array();
            // $scope.CTresponseData[index].abstract_highlight = $scope.CTresponseData[index].brief_summary;
			$scope.highlights.forEach( function (arrayItem){
				if(arrayItem.value === 'Show'){
					if($scope.CTresponseData[index].hasOwnProperty(arrayItem.entitiesField)){
						if(arrayItem.label === 'Sentences') { //HIGHLIGHT SENTENCES
                            for (var i = $scope.CTresponseData[index][arrayItem.modelField].length; i > 0; i--) {
                                var positions = JSON.parse($scope.CTresponseData[index][arrayItem.modelField][i - 1]);
                                startText = $scope.CTresponseData[index].abstract_highlight.substring(0, positions[0]);
                                slicedText = $scope.CTresponseData[index].abstract_highlight.substring(positions[0], positions[1]);
                                endText = $scope.CTresponseData[index].abstract_highlight.substring(positions[1]);
                                $scope.CTresponseData[index].abstract_highlight = startText + "<span style=\"background-color: rgba(255,255,0," + positions[2] + ")\">" + slicedText + "</span>" + endText;
                            }
                            $scope.CTresponseData[index].abstract_highlight=$sce.trustAsHtml($scope.CTresponseData[index].abstract_highlight);
                        } else { // HIGHLIHGT OTHERS
							$scope.CTresponseData[index][arrayItem.entitiesField].map( function(annItem) {
								$scope.CTresponseData[index].abstract_highlight= $scope.highlightAnnotations($sce.valueOf($scope.CTresponseData[index].abstract_highlight),annItem,arrayItem.css);
							});
						}
					} // HIGHLIHGT SENTENCES
				}
			});
		}, true);
	console.log($scope.CTresponseData[index].abstract_highlight);
	};

	$scope.expandCallback = function (index, id){
		$scope.responseData[index].abstract_highlight=$scope.responseData[index].abstract;
		$scope.$watch('highlights', function () {
			var offset=0;
			positionArray= Array();
			$scope.responseData[index].abstract_highlight=$scope.responseData[index].abstract;

    		$scope.highlights.forEach( function (arrayItem){
				if(arrayItem.value === 'Show'){
					if($scope.responseData[index].hasOwnProperty(arrayItem.modelField)){
						for(var i =  $scope.responseData[index][arrayItem.modelField].length; i > 0; i--){
							var positions=JSON.parse($scope.responseData[index][arrayItem.modelField][i-1]);
							var start_token=null;
							if (arrayItem.label === 'Sentences') {
								start_token = "<span style=\"background-color:rgba(255,255,0,"+positions[2]+")\">";
							} else {
								start_token = "<span style=\"background-color:"+arrayItem.css+"\">";
							}
							positionArray.push([positions[0], positions[1], start_token]);
						}
					}
				}
			});

            console.log($scope.responseData[index], $scope.responseData[index].abstract.length, $scope.responseData[index].abstract_highlight.length);

            positionArray = positionArray.sort(function(a, b) {
                // console.log(a, b);
                // return a[0].localeCompare(b[0]);
                return a[0] < b[0];
            });

            console.log(positionArray);

            positionArray.forEach( function (arrayItem) {
                console.log(arrayItem);
                // CHANGING THE COLOR
                startText=$scope.responseData[index].abstract_highlight.substring(0, arrayItem[0]);
                slicedText=$scope.responseData[index].abstract_highlight.substring(arrayItem[0], arrayItem[1]);
                endText=$scope.responseData[index].abstract_highlight.substring(arrayItem[1]);
                $scope.responseData[index].abstract_highlight = startText+arrayItem[2]+slicedText+"</span>"+endText;
            });

		}, true);


		// console.log('expanded pane:', index, id);
  		// console.log($scope.responseData[index]);
  		// console.log($scope.responseData[index].abstract);
        //
  		// if($scope.responseData[index].hasOwnProperty('sents')){
        //     if(!$scope.responseData[index].hasOwnProperty('abstract_highlight')){
        //         console.log($scope.responseData[index].sents);
        //         tmpText = $scope.responseData[index].abstract;
        //
        //         for(var i =  $scope.responseData[index].sents.length; i > 0; i--){
        //             console.log(i);
        //             // console.log($scope.responseData[index].sents[i]);
        //             // console.log(typeof $scope.responseData[index].sents[i]);
        //             // console.log(JSON.parse($scope.responseData[index].sents[i]));
        //             var positions=JSON.parse($scope.responseData[index].sents[i-1]);
        //             console.log(positions, typeof positions);
        //
        //             startText=tmpText.substring(0, positions[0]);
        //             slicedText=tmpText.substring(positions[0], positions[1]);
        //             endText=tmpText.substring(positions[1]);
        //             tmpText = startText+"<span style=\"background-color: rgba(255,255,0,"+positions[2]+")\">"+slicedText+"</span>"+endText;
        //             console.log(slicedText, positions);
        //             console.log(tmpText);
        //         }
        //         $scope.responseData[index].abstract_highlight=tmpText;
        //     }
  		// } else{
  		//     $scope.responseData[index].abstract_highlight=$scope.responseData[index].abstract;
        // }

	};

	$scope.collapseCallback = function (index, id) {
  		// console.log('collapsed pane:', index, id);
		$scope.responseData[index].abstract_highlight=null;
	};

	$scope.collapseCallbackCT = function (index, id) {
  		// console.log('collapsed pane:', index, id);
		$scope.CTresponseData[index].abstract_highlight=null;
	};

	$scope.trustAsHtml = function(string) {
    return $sce.trustAsHtml(string);
    };

	// MULTIPLE CHECKBOXES
	  $scope.highlights=[
	  		{label:'Sentences', value:'Show', modelField:'sents', entitiesField:'', css:'rgb(255,255,0,0.4)'},
		   	{label:'Genes', value:'Show', modelField:'genes_pos', entitiesField:'gene_name',css:'rgba(0,153,255,0.4)'},
		   	{label:'Mutations', value:'Show', modelField:'mut_pos', entitiesField:'mutation_normalizedValue',css:'rgba(153,204,0,0.4)'},
		   	{label:'Chemicals', value:'Show', modelField:'chem_pos', entitiesField:'chemicals',css:'rgba(0,204,153,0.4)'}
		];

	  $scope.selection=[];
	  // toggle selection for a given employee by name
	  $scope.toggleSelection = function toggleSelection(employeeName) {
		 var idx = $scope.selection.indexOf(employeeName);

		 // is currently selected
		 if (idx > -1) {
		   $scope.selection.splice(idx, 1);
		 }

		 // is newly selected
		 else {
		   $scope.selection.push(employeeName);
		 }

		 // console.log($scope.selection);
		 // console.log($scope.highlights);


	   };

	  $scope.openLink = function(idItem, field){

	  	// console.log(idItem, field);

	  	switch(field) {
			case 'gene':
				url = "https://www.ncbi.nlm.nih.gov/gene/"+idItem.split('|')[1];
				break;
			case 'mutation':
				url = "https://www.ncbi.nlm.nih.gov/gene/"+idItem.split('|')[1];
				break;
			case 'chemicals':
				url = "https://www.drugbank.ca/drugs/"+idItem.split('|')[1];
				break;
				}

			$window.open(url, '_blank');

  		};


	  $scope.displayTabResults = function (label) {
	  	$scope.showTab= label;
	  	$scope.currentNavItem = 'page1';
      };

}]);

app.filter('unsafe', function($sce) { return $sce.trustAsHtml; });
app.filter('split', function() {
        return function(input, splitChar, splitIndex) {
            // do some bounds checking here to ensure it has that index
            return input.split(splitChar)[splitIndex];
        }
    });