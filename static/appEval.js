//var history_api = typeof history.pushState !== 'undefined';
//if ( history_api ) history.pushState(null, '', '#StayHere');

// Define the `phonecatApp` module
var app = angular.module(
		'classification', 
		['ngRoute','ngAnimate','ngMaterial','ngAria','ngMessages', 'angAccordion', 'rzModule', 
		 'angular-loading-bar','angucomplete-alt', 'ngMdIcons', 'md.data.table', 'angularUtils.directives.dirPagination', 'vAccordion','ngDialog']
);

  
app.config(function ($interpolateProvider) {$interpolateProvider.startSymbol('[[').endSymbol(']]')});

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

app.config(function($mdThemingProvider){
    // Update the theme colors to use themes on font-icons
    $mdThemingProvider.theme('default')
          .primaryPalette("red")
          .accentPalette('green')
          .warnPalette('blue');
  	    //.backgroundPalette('white');


    $mdThemingProvider.theme('custom')
	    .primaryPalette('grey')
	    .accentPalette('deep-purple')
	    .warnPalette('green');
    
	    $mdThemingProvider.theme('forest')
	    .primaryPalette('brown')
	    .accentPalette('green')
        .warnPalette('cyan')
	    .backgroundPalette('light-blue');
  });

app.config(function($mdIconProvider) {
    $mdIconProvider
    .iconSet('social', 'img/icons/sets/social-icons.svg', 24)
    .defaultIconSet('img/icons/sets/core-icons.svg', 24);
});
  
app.controller('lala', ['$scope', '$log', '$http', '$timeout', '$mdSidenav','$window',
				function($scope, $log, $http, $timeout, $mdSidenav,$window,ngDialog) {
	
	//DISABLE BACK BUTTON
//	$scope.$on('$locationChangeStart', function(event, next, current){            
//	    // Here you can take the control and call your own functions:
//	    //alert('Sorry ! Back Button is disabled');
//	    // Prevent the browser default action (Going back):
//	    event.preventDefault();            
//	});	
	
	/*
	 * INITIALIZE EVERYTHING NEEDED TO
	 */
	$scope.init = function() {
			//$scope.resetValues();
			//$scope.autoSaveFeedback = 10;
			$scope.feedbackData = [];
			$scope.selectedQuery=null;
			 $scope.message = "Please, execute a query from the list above.";
			console.log('Everything set up!');			
		};	
		
		/*
		*	APP VARIABLES
		*/
		$scope.journals = [];
		$scope.journalsTmp = []
		$scope.selectedJournals = [];
		$scope.selectedJournal = null;
		$scope.feedbackData = [];
		$scope.classificationData = [];
		$scope.responseData = [];
		$scope.responseDataContainer = [];		
		$scope.lastQueryID=-1;
		$scope.currentPage=1;		  
		 $scope.isShown = true;
		 $scope.isShownFeedback = false;
		 $scope.sourceCount = null;
		 $scope.selectedTab=0;
		 $scope.civicJournals = false;
		 $scope.chooseCivic = false;
		 $scope.queryError=false;
		 $scope.noResults=false;
		 $scope.feedbackData = [];
		 $scope.evaluatedPmids = [];
		 $scope.autoSaveFeedback = 10;
		 $scope.message = "Welcome! Please, execute a query from the list above.";
		 $scope.selectedQuery=null;
		 $scope.isTrue=false;
		 $scope.evaluatedPmids =[];
		 $scope.queriesMessage = 'EVALUATION QIERIES';
		 $scope.disableSubmission = true;
		 $scope.showRelevant = true;
		 $scope.storeMessage = "";
		
		/*
		 * QUERY OBJECT 
		 */
		$scope.queryTerms={
				'genes':'',
				'mutations':'',
				'minYear':$scope.minValue ,
				'maxYear':$scope.maxValue,
				'currentPage':$scope.currentPage,
				'nrDocuments':100,
				'cancerType':'',
				'queryID':$scope.lastQueryID,
				//'userID':$scope.userId,
				'journals':$scope.selectedJournals,
				};

	
	$scope.getEvaluationQueries = function(){
		
		  $http({
		         method: 'GET',
		         url: '/getEvaluationQueries',
		         headers: {
				        'Content-Type': 'application/json'
				    }
		     }).then(function(response) {

		          $scope.selectedQuery=null;
		          $scope.queriesData = response.data;
		          console.log($scope.queriesData);
		          
		          if($scope.queriesData.length == 0){
		     		 $scope.message = "You have evaluated all queries! Thank you for participating";
		          }
		          
		          //console.log('evaluationQueries ' + $scope.responseData);
				  
		     }, function(response) {
		          alert('There was an error in the query');
		    });
		  
		  $scope.message = "Please, execute a query from the list above.";

	};
	
		//AJAX TOP CIVIC
		$scope.getTopCivic = function () {	
		  $http({
		         method: 'GET',
		         url: '/getCivicEval',
		         headers: {
				        'Content-Type': 'application/json'
				    }
		     }).then(function(response) {
		    	 //console.log(response.data);
		    	//responseData = response.data['docs'];
		    	$scope.selectedJournals=response.data.map(function(a) {return a.name;});
		     }, function(response) {
		          $scope.status = response.status;
		    });
		};
		
		//ALREADTY EVALUATED ARTICLES - BACKGROUND COLOR
		$scope.alreadyEvaluated = function(pmid){

			var storeFeeback=true;
    		
			if ($scope.evaluatedPmids.indexOf(pmid) != -1) {
	    		$scope.feedbackData.forEach( function (arrayItem, index, object) {
//	    			console.log(arrayItem['pmid'] + '\t'+ typeof(arrayItem['pmid']) );
						if(arrayItem['pmid'] == pmid)  {
//							console.log('Found it');
							if (arrayItem['useful'] == null ||   arrayItem['classification'] ==null ){
	    				    	storeFeeback = false;								
	    				    	return false;
	    				    } else {
	    				    	return true;
	    				    }
						}
	    		});				
		} else{
				storeFeeback = false;
				return false;
			};    		
    		
    		
    		//console.log(storeFeeback);
    		return storeFeeback;
    		
		};
		
		//already selected options		
		$scope.preselect = function(pmid, field, value){
			
			var storeFeeback=true;    		
			if ($scope.evaluatedPmids.indexOf(pmid) != -1) {
	    		$scope.feedbackData.forEach( function (arrayItem, index, object) {
						if(arrayItem['pmid'] == pmid)  {
							if (arrayItem[field] != value){
	    				    	storeFeeback = false;
	    				    }
						}
	    		});
			} else{
					storeFeeback = false;
			};

			console.log(pmid, field,storeFeeback);
    		return storeFeeback;
		}

    /*
     * DUMMY SEARCH FUNCTION  
     */
	$scope.searchFunction = function (queryObject) {	

		  $http({
		         method: 'GET',
		         url: '/getQueryEval',
		         params: queryObject,
		         headers: {
				        'Content-Type': 'application/json'
				    }
		     }).then(function(response) {
		    	 $scope.status = response.status;
		          $scope.responseData = response.data['docs'];
		          console.log("Number of returned documents\t" + $scope.responseData.length);
		          $scope.autoSaveFeedback = $scope.responseData.length;

		          if(response.data['alreadyEvaluated'] != null){
		        	  $scope.feedbackData = response.data['alreadyEvaluated'];
		        	  $scope.evaluatedPmids = $scope.feedbackData.map(function(a) {return a.pmid;});
//		        	  console.log('$scope.evaluatedPmids '+$scope.evaluatedPmids );
//		        	  console.log('$scope.evaluatedPmids type '+typeof($scope.evaluatedPmids));
		        	} else {
		        		$scope.feedbackData = [];
		        	}

		          $scope.message = '';
		     }, function(response) {
		          $scope.message = 'There was an error. Please try again.';
		    });

  		};
  		
		 //FILTER FUNCTION
	  	$scope.filter = function(index){
	  		$scope.message = "Please wait while the query executes. It won't take long, don't go away.";
	  		$scope.selectedQuery=index;
	  		$scope.responseData=[];
  			$scope.queryTerms['queryID']=$scope.queriesData[index].evaluationQueries_id;
  			$scope.queryTerms['genes'] = $scope.queriesData[index].evaluationQueries_genes;
  			$scope.queryTerms['mutations']=$scope.queriesData[index].evaluationQueries_mutations;
  			$scope.queryTerms['minYear']=$scope.queriesData[index].evaluationQueries_yearsFrom;
  			$scope.queryTerms['maxYear']=$scope.queriesData[index].evaluationQueries_yearsTo;
  			$scope.queryTerms['minFiltered']=$scope.queriesData[index].evaluationQueries_yearsFrom;
  			$scope.queryTerms['maxFiltered']=$scope.queriesData[index].evaluationQueries_yearsTo;
  			$scope.queryTerms['cancerType']=$scope.queriesData[index].evaluationQueries_cancerType;
			$scope.queryTerms['journals']=$scope.selectedJournals;
	  		$scope.searchFunction($scope.queryTerms);
	  		$scope.queriesMessage = 'CURRENT ACTIVE QUERY';
	  	};

 /*
 * FEEDBACK FOR RESULTS - ADD AND REMOVE ITEMS FROM FEEDBACK LIST
 */
    $scope.addRelevance = function(pmid, value, qId, type) {

    	var notFound = true;
        for(var i=0; i < $scope.feedbackData.length; i++) {
            if($scope.feedbackData[i].pmid == pmid) {
            	if (type == 'relevant'){
            		$scope.feedbackData[i].useful=value;
            	} else {
            		$scope.feedbackData[i].classification=value;
            	};
            	notFound = false;
                break;
            }
        }

        if(notFound){
        	if (type == 'relevant'){
        		$scope.feedbackData.push({'pmid':pmid,'useful':value,'classification':null,'queryId':qId});
        	} else {
        		$scope.feedbackData.push({'pmid':pmid,'useful':null,'classification':value,'queryId':qId});
        	};        	        	
        }

        $scope.selectedOption = null;
        $scope.selectedOptionCorrect = null;

    };
        
    $scope.removeItem = function(pmid) {
    	for(var i=0; i < $scope.feedbackData.length; i++) {
            if($scope.feedbackData[i].pmid == pmid) {
            	$scope.feedbackData.splice(i, 1);
                break;
            }
        }
    };
    
    /*
     * FEEDBACK WATCHER
     */
    $scope.$watch('feedbackData', function() {
    	
    		console.log('feedbackData changed'+$scope.feedbackData.length );
	    	if  ($scope.feedbackData.length == $scope.autoSaveFeedback ){

	    		//$scope.feedback(0); //AUTOMATIC SAVE

	    		//ENABLE SAVE - MANUAL SAVE
	    		var storeFeeback = true;    		
	    		$scope.feedbackData.forEach( function (arrayItem, index, object) {
	    				for (var key in arrayItem) {
	    				    if (arrayItem[key]==null){
	    				    	storeFeeback=false;
	    				    }
	    				}
	    		});
	    		
	    		if(storeFeeback){
	    			$scope.disableSubmission = false;
	    			$scope.isShownFeedback=true;
	    			$scope.storeMessage = "Everything evaluated! You can store your feedback with the Save Feedback (Mark Query as Evaluated) button";
	    			$scope.accordion.collapseAll();
//	    			$scope.showRelevant = false;
	    		};
	    		
	    	} else {
				$scope.disableSubmission = true;
				$scope.storeMessage = "";
//				$scope.showRelevant = true;
			};
	    			
	    	if  ($scope.feedbackData.length == 0 ){
	    		$scope.isShownFeedback = false;
	    		};
	    		
	    	if  ($scope.feedbackData.length > 0 ){
		    		$scope.evaluatedPmids = $scope.feedbackData.map(function(a) {return a.pmid;});
		    		$scope.radioButtonValues = {};
		    		$scope.feedbackData.forEach( function (arrayItem, index, object) {
//		    			console.log('object\t'+object);
//		    			console.log('arrayItem\t'+arrayItem);
//		    			console.log(object.pmid);
//		    			console.log(arrayItem.pmid);
		    			$scope.radioButtonValues[arrayItem.pmid]= {'useful':arrayItem.useful, 'classification':arrayItem.classification};
		    		});
	    		};
    }, true);

    /*
     * EVALUATION QUERIES WATCHER
     */
    
//    $scope.$watch('queriesData', function() {
//    	
//    	
//    }
    
    
    //save user feedback
    $scope.feedback=function (feedbackType) {

    		var storeFeeback = true;    		
    		$scope.feedbackData.forEach( function (arrayItem, index, object) {
    				for (var key in arrayItem) {
    				    if (arrayItem[key]==null){
    				    	storeFeeback=false;
    				    }
    				}
    		});

    		if(storeFeeback){

    		      $scope.feedbackResponse = null;
    		      $scope.feedbackStatus = null;
    		      
    		  	  $http({
    		  	         method: 'GET',
    		  	         url: '/feedbackEval',
    		  	         params:{'feedbackData':$scope.feedbackData, 'feedbackType':feedbackType},
    		  	         headers: {
    		  			        'Content-Type': 'application/json'
    		  			    }
    		  	     }).then(function(response) {
    		  	          $scope.feedbackResponse = response.data;
    		  	     }, function(response) {
    		  	          $scope.feedbackResponse = response.data || 'Request failed';
    		  	          $scope.message = 'Request failed';
    		  	    });
    		  	  
    		  	  $scope.feedbackData = [];
    		  	  $scope.responseData = [];
    		  	  $scope.getEvaluationQueries();
    		  	  $scope.queriesMessage = 'EVALUATION QIERIES';

    		  	  if(feedbackType == 0){
    		  		  $scope.message = "Evalation results saved. Please, execute another query from the list above.";
    		  	  } 
    		}
    };    
    
    /*
     * CONTINUE EVALUATION
     */
    
    $scope.continueEvaluation=function(){
    	$scope.showRelevant = true;
    }

    
  	/*
  	 * LOGOUT
  	 */
  	$scope.logout = function(){

  		if  ($scope.feedbackData.length > 0){
  			$scope.feedback(1);
  	  		alert("Evalation results saved. You can resume your evaluation at a later date!");
  		};
  		
  		$window.location.href = '/logout';

  	}; 
  	
  	$scope.switchCollapse = function(){
  			$scope.isShown = !$scope.isShown;
  	};

  	$scope.toggleViewFeedback = function(){
			$scope.isShownFeedback = !$scope.isShownFeedback;
	};
  	
}]);
