import tornado.ioloop
import tornado.web
import tornado.httpserver
from tornado import gen
# import tormysql

from tornado.httpserver import HTTPServer
from tornado.web import RequestHandler

import MySQLdb as mdb
# import torndb
# from sqlalchemy import create_engine
import os
import uuid
import sys
from urllib2 import *
import ssl
import cPickle as pickle

sys.path.insert(0, '../')
from _config import *

#===============================================================================
# INSTALL DB IF DOESNT EXIST
#===============================================================================
databases = {
    db:DB_DUMP,
    dbEval:DB_DUMP_EVAL
}

for k, v in databases.iteritems():

    try:
        con = mdb.connect('localhost', USER, PASS, k, use_unicode=True, charset='utf8')
        print "Database {} exisits. Starting server.".format(k)
    except mdb.OperationalError, e:
        #===========================================================================
        # DB DOES NOT EXIST
        #===========================================================================
        print "Creating database"
        con = mdb.connect('localhost', USER, PASS,  use_unicode=True, charset='utf8')
        proc = subprocess.Popen(["mysql", "--user=%s" % USER, "--password=%s" % PASS],
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE)
        out, err = proc.communicate(file(v).read())

# MAIN APP: HANDLERS ET AL
class Application(tornado.web.Application):
    # def make_app():
    def __init__(self):

        settings = {
            "template_path": os.path.join(os.path.dirname(__file__), "templates"),
            "static_path": os.path.join(os.path.dirname(__file__), "static"),
            "debug" : True,
            'autoreload':True,
            'cookie_secret':str(uuid.uuid4()),
            'debug':True,
            "xsrf_cookies": True,
            "login_url": "/login",
            }

        handlers =[
                (r"/login", LoginHandler),
                (r"/logout", LogoutHandler),

                #VIST APP
                (r"/", MainHandler),
                (r"/help", HelpHandler),
                (r"/getQuery", QueryHandler),
                (r"/getCivic", CivicHandler),
                (r"/feedback", FeedbackHandler),
                (r"/journals", JournalsHandler),
                (r"/getQueriesExamples", exampleQueriesHandler),

                # EVAL MODULES
                (r"/eval", MainHandlerEval),
                (r"/getQueryEval", QueryHandlerEval),
                (r"/getEvaluationQueries", evaluationQueriesHandler),
                (r"/getCivicEval", CivicHandlerEval),
                (r"/feedbackEval", FeedbackHandlerEval)
            ]

        # application = tornado.web.Application(handlers,**settings)
        # tornado.web.Application.__init__(self, handlers,**settings)
        super(Application, self).__init__(handlers, **settings)

        self.con = mdb.connect('localhost', USER, PASS, db, use_unicode=True, charset='utf8', )
        self.con.autocommit(True)
        self.cur = con.cursor(mdb.cursors.DictCursor)

        # self.db = torndb.Connection(
        #     host='localhost', database=db, user=USER, password=PASS
        # )

        # self.pool = tormysql.ConnectionPool(
        #     max_connections=256,  # max open connections
        #     idle_seconds=7200,  # conntion idle timeout time, 0 is not timeout
        #     wait_connection_timeout=5,  # wait connection timeout
        #     host="localhost",
        #     user=USER,
        #     passwd=PASS,
        #     db=db,
        #     cursorclass = tormysql.cursor.DictCursor,
        #     autocommit = True,
        #     use_unicode = True,
        #     charset = "utf8"
        # )

        self.journalList = pickle.load(open(JOURNAL_PICKLE, 'rb'))
        self.journalListInverted = pickle.load(open(JOURNAL_PICKLE_INVERTED, 'rb'))
        print 'Loaded Journal List'

        self.reqPage='http://localhost:8983/solr/{}/select'.format(CORE_NAME)
        self.reqPageCT='http://localhost:8983/solr/ctg/select'
        self.headers = {
         'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36'
        }

# class BaseHandler(tornado.web.RequestHandler):
#     def get_current_user(self):
#         return self.get_secure_cookie("user")
#
#     @property
#     def con(self):
#         return self.application.con
#
#     @property
#     def cur(self):
#         return self.application.cur
#
#     @property
#     def journalList(self):
#         return self.application.journalList
#
#     # @property
#     def journalListInverted(self):
#         return self.application.journalListInverted
#
#     # @property
#     def getReqPage(self):
#         return self.application.reqPage

#INCLUDE WEB APP CODE
from webApp import *

# INCLUDE EVAL APP CODE
from evalApp import *

if __name__ == "__main__":

    ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_ctx.load_cert_chain("keys/cert-8971959085156491888260191656.pem", "keys/triage_nopass.key")
    # ssl_ctx.load_verify_locations("keys/cacert.crt")
    # ssl_ctx.verify_mode = ssl.CERT_REQUIRED
    app = Application()

    http_server = HTTPServer(app, ssl_options=ssl_ctx)
    http_server.listen(5000)
    http_server.start()
    tornado.ioloop.IOLoop.current().start()
