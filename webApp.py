import tornado.ioloop
import tornado.web
import tornado.httpserver
from  tornado.escape import json_decode
from tornado import gen
import requests
import MySQLdb as mdb
import traceback
import json
import ast
from operator import itemgetter
from collections import Counter
import operator
import pandas as pd
from concurrent.futures import ThreadPoolExecutor
import pprint
import sys
from random import randrange
from urllib2 import *
from tornado import template
#from tornado.httpserver import HTTPServer
import numpy as np
import time
import os
import uuid
from urllib import urlencode
import simplejson
import ssl
import cPickle as pickle

# USER = 'root'
# PASS=''
# db = 'cancer'
# dbEval = 'vistEvaluation'

sys.path.insert(0, '../')
from _config import *

class BaseHandler(tornado.web.RequestHandler):

    def get_current_user(self):
        return self.get_secure_cookie("user")

    # @property
    # def db(self):
    #     # print self.application.db
    #     return self.application.db

    @property
    def con(self):
        # print self.application.con
        return self.application.con

    @property
    def cur(self):
        # print self.application.cur
        return self.application.cur

    @property
    def journalList(self):
        # print self.application.journalList
        return self.application.journalList

    @property
    def journalListInverted(self):
        return self.application.journalListInverted

    @property
    def reqPage(self):
        return self.application.reqPage

    @property
    def reqPageCT(self):
        return self.application.reqPageCT

    @property
    def headers(self):
        return self.application.headers

class MainHandler(BaseHandler):

    def get(self):
        # if not self.current_user:
        #     self.redirect("/login")
        #     return
        self.current_user='0'
        tornado.escape.xhtml_escape(self.current_user)
        # self.write("Hello, " + name)
        self.render("index.html", title="Variant Information Search Engine (VIST)")

class HelpHandler(BaseHandler):

    def get(self):
        self.render("help.html", title="VIST - Info and Help")

class LoginHandler(BaseHandler):
    def get(self):
        self.render("login.html", title="Log in", error="")

    def post(self):
        getusername = tornado.escape.xhtml_escape(self.get_argument("username"))
        getpassword = tornado.escape.xhtml_escape(self.get_argument("password"))
        # application = tornado.escape.xhtml_escape(self.get_argument("application2Use"))
        user = None

        try:

            # if application == '2':
            #     con = mdb.connect('localhost', USER, PASS, db, use_unicode=True, charset='utf8')
            #
            # if application == '1':
            con = mdb.connect('localhost', USER, PASS, dbEval, use_unicode=True, charset='utf8')
            cur = con.cursor(mdb.cursors.DictCursor)
            query = "SELECT * from user where user_email='{}' and user_pass='{}'".format(getusername, getpassword)
            cur.execute(query)

            if cur.rowcount != 1:
                # self.redirect("/login", title="Log in carefully", error='Wrong username and/or password')
                self.render("login.html", title="Log in carefully", error='Wrong username and/or password')
                return

            # print user
            user = cur.fetchall()[0]
            self.set_secure_cookie("user", str(user['iduser']))

        except mdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            print query
            traceback.print_exc()

        con.close()
        # if application == '2':
        #     self.redirect("/")
        #
        # if application == '1':
        self.redirect("/eval")

class LogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.clear_cookie("name")
        self.clear_cookie("lastname")
        self.redirect("/")

class QueryHandler(BaseHandler):

    executor = ThreadPoolExecutor(20)

    @tornado.concurrent.run_on_executor
    def prepareAnnotations(self, solrField, data):
        queryAnnotation = ''
        if data:
            try:
                queryTerms = ['"'+item.strip()+'"' for item in data.split(',') if item]
                if len(queryTerms)>1:
                    annotationList = ' OR '.join(queryTerms)
                elif len(queryTerms) == 1:
                    annotationList = queryTerms[0]

                if solrField:
                    queryAnnotation = '{}:({})'.format(solrField, annotationList)
                else:
                    queryAnnotation = '({})'.format(annotationList)
            except Exception, e:
                print e
                print traceback.print_exc()
        return queryAnnotation

    @tornado.concurrent.run_on_executor
    def executeQuery(self, query):
        res = None
        try:
            con = mdb.connect('localhost', USER, PASS, db, use_unicode=True, charset='utf8')
            cur = con.cursor(mdb.cursors.DictCursor)
            cur.execute(query)
            con.commit()
            res = cur.lastrowid
            con.close()
        except Exception as e:
            traceback.print_exc()
            print e
            print query
            print '------'

        return res

    @tornado.concurrent.run_on_executor
    def executeQuery_Permanent(self, query):
        tx = yield pool.begin()
        try:
            yield tx.execute(query)
        except:
            yield tx.rollback()
        else:
            yield tx.commit()

        cursor = yield pool.execute("SELECT * FROM test")
        datas = cursor.fetchall()

        # print datas

        yield pool.close()

    @tornado.concurrent.run_on_executor
    def filterCancerType(self, data):
        cancerTypeFilter = None
        try:
            cancerType = data['cancerType'][0]
            if cancerType != 'any' and cancerType != '' and cancerType != 'all' :
                cancerTypeFilter='cancerType:"{}"'.format(cancerType)

            if cancerType == 'all':
                cancerTypeFilter='-cancerType:"Not cancer"'

        except KeyError:
            pass

        return cancerTypeFilter

    @tornado.concurrent.run_on_executor
    def getYearsMinMax(self, queryObject, nrDocs):
        queryObject['fl']='pubdate'
        queryObject['sort']='pubdate desc'
        queryObject['rows']=nrDocs
        queryObject['start']=0
        r = requests.get(self.reqPage, headers=self.headers, params=queryObject)
        response = r.json()
        candidates = [x['pubdate'] for x in response['response']['docs'] if len(str(x['pubdate'])) == 4]
        return (max(candidates), min(candidates))
        #self.finish()

    @tornado.concurrent.run_on_executor
    def getJournals(self, queryObject, nrDocs):
        queryObject['fl']='journal, source'
        queryObject['sort']=''
        queryObject['rows']=nrDocs
        queryObject['start']=0
        r = requests.get(self.reqPage, headers=self.headers, params=queryObject)
        response = r.json()
        journals = []
        data = response['response']['docs']
        #print Counter([item['journal'] for item in response['response']['docs']])
        x = dict(Counter([ item['journal'] for item in data]))
        #x = dict(sorted(x.items(), key=operator.itemgetter(1), reverse=True))
        #print x
        #raw_input('prompt')
        journals = [{'name':k, 'titleName': k +' (' + str(v) + ')', 'countValue':v} for k,v in x.iteritems() if k.strip() in self.journalList]
        journals = sorted(journals, key=itemgetter('countValue'), reverse=True)

        #=======================================================================
        # COUNT PER SOURCE -- CURRENTLY NOT USED
        #=======================================================================
        sourceCount = {
            'all':0,
            "pubmed":0,
            #"ASCO":0,
            # "pmc":0
        }

        source = [item['source'] for item in data]
        sourceCountTmp = dict(Counter(source))
        sourceCountTmp['all']=len(source)
        sourceCount.update(sourceCountTmp)
        sourceCount=dict(sorted(sourceCount.items(), key=operator.itemgetter(1), reverse=True ))
        sourceCount=[{'source':k, 'occurences': v} for k,v in sourceCount.iteritems()]
        return (journals, sourceCount)
        #self.finish()

    @tornado.concurrent.run_on_executor
    def filterJournals(self, data):
        # FILTER JOURNALS IF POSSIBLE
        journalFilter=None
        if 'journals' in data:
            # print data['journals']
            if len(data['journals'])==1:
                journalFilter = '"'+data['journals'][0]+'"'
                #params["fq"] = 'journal={}'.format(journalFilter)
            else:
                journalFilter = ' OR '.join(['"'+item+'"' for item in data['journals']])
            journalFilter = 'journal:({})'.format(journalFilter)
            # print journalFilter
        return journalFilter

    @tornado.concurrent.run_on_executor
    def queryMedline(self, params, data):
        # r = requests.get(reqPage, headers=headers, params=params)
        resList ={
                'docs':None,
                'ct': None,
                'numFound':0,
                'journals': [],
                'queryID':[],
                'minPublication':0,
                'maxPublication':0
            }

        r = requests.post(self.reqPage, data=params)
        response = r.json()

        # ===================================================================
        # WRITE QUERY TO DB
        # ===================================================================
        try:
            queryID = data['queryID'][0]
        except KeyError:
            query = """INSERT INTO cancer.queries
                        (queries_userID, queries_genes, queries_mutations)
                        VALUES ('{}', '{}', '{}')""".format(userID, self.con.escape_string(genes),
                                                            self.con.escape_string(mutations))
            queryID = yield self.executeQuery(query)

        query = """INSERT INTO  queriesResult
                  (queriesResult_userid,queriesResult_queryID,queriesResult_resultsPage,queriesResult_pmids)
                   VALUES ('{}','{}','{}','{}')""".format(userID, queryID, (int(data['currentPage'][0]) - 1),
                                                          ",".join([x['pmid'] for x in response['response']['docs']]))
        res = yield self.executeQuery(query)

        if response['response']['numFound'] > 0:
            journals, sourceCount = yield self.getJournals(params, response['response']['numFound'])
            # if 'source' in data:
            #     sourceCount = [json.loads(item) for item in data['sourceCount'] ]

            if 'queryID' not in data:
                maxTemp, minTemp = yield self.getYearsMinMax(params, response['response']['numFound'])
                min = minTemp
                max = maxTemp
                minFilter = minTemp
                maxFilter = maxTemp
            else:
                min = int(data['minYear'][0])
                max = int(data['maxYear'][0])
                minFilter = int(data['minFiltered'][0])
                maxFilter = int(data['maxFiltered'][0])

            resList = {
                'docs': response['response']['docs'],
                # 'numFound':response['response']['numFound'],
                'numFound': sourceCount[0]['occurences'],
                'journals': journals,
                # 'sourceCount':sourceCount,
                # 'queryID':queryID,
                'minPublication': min,
                'maxPublication': max,
                'minPublicationFilter': minFilter,
                'maxPublicationFilter': maxFilter
            }

        yield resList
        return

    @tornado.concurrent.run_on_executor
    def queryCTG(self, data, keywords, genes, mutations):

        # print keywords, genes, mutations

        params={
          'group':'true',
          'group.field':'nct_id',
          'group.main':'true',
          'q.op': 'OR',
          "indent":"on",
          "sort":"query($q) desc",
          # "sort": "classification_score desc",
          "wt":"json",
          "rows": data['nrDocuments'][0],
          'start':int (data['nrDocuments'][0])* (int(data['currentPage'][0]) -1 ),
          'q':'',
          'fl':'*, score',
          'fq': ''
        }

        # print [keywords, genes, mutations]
        # raw_input('args')

        resList=[]
        params["q"] = ' AND '.join([x for x in [keywords, genes, mutations] if x])
        params["fq"] = ' AND '.join([x for x in [genes, mutations] if x])
        r = requests.post(self.reqPageCT, data=params)
        # print r
        response = r.json()

        if response['response']['numFound'] > 0:
            resList = response['response']['docs']

        return resList, response['response']['numFound']


# class QueryHandler(BaseHandler):

    # @tornado.web.authenticated
    # @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):

        #=======================================================================
        # REQUEST DATA
        #=======================================================================
        data = self.request.arguments
        # print self.request
        # print(dir(data))
        # pprint.pprint(data)

        params={
          'group':'true',
          'group.field':'pmid',
          'group.main':'true',
          'q.op': 'OR',
          "indent":"on",
          "sort":"product(query($q),classification_score) desc",
          # "sort": "classification_score desc",
          "wt":"json",
          "rows": data['nrDocuments'][0],
          'start':int (data['nrDocuments'][0])* (int(data['currentPage'][0]) -1 ),
          'q':'',
          'fl':'*, score',
            'fq':[]
        }

        # userID = tornado.escape.xhtml_escape(self.current_user)
        userID=0
        resList ={
                'docs':None,
                'ct': None,
                'numFound':0,
                'journals': [],
                'queryID':[],
                'minPublication':0,
                'maxPublication':0
            }

        params={
          'group':'true',
          'group.field':'pmid',
          'group.main':'true',
          'q.op': 'OR',
          "indent":"on",
          "sort":"product(query($q),classification_score) desc",
          # "sort": "classification_score desc",
          "wt":"json",
          "rows": data['nrDocuments'][0],
          'start':int (data['nrDocuments'][0])* (int(data['currentPage'][0]) -1 ),
          'q':'',
          'fl':'*, score',
            'fq':[]
        }

        response = {}
        qParams = []

        #=======================================================================
        # QUERY PARAMS PARSING
        #=======================================================================
        keywords =  yield self.prepareAnnotations(None, data['keywords'][0])
        qParams.append(keywords)

        genes =  yield self.prepareAnnotations('gene_name', data['genes'][0])
        qParams.append(genes)

        mutations = yield self.prepareAnnotations('mutation_normalizedValue', data['mutations'][0])
        qParams.append(mutations)

        journalFilter = yield self.filterJournals(data)
        # qParams.append(journalFilter)

        #cancer type
        cType = yield self.filterCancerType(data)
        # qParams.append(cType)

        #FILTER YEARS IF POSSIBLE
        try:
            qParams.append('pubdate:[{} TO {}]'.format(data['minFiltered'][0], data['maxFiltered'][0]))
        except KeyError, e:
            #traceback.print_exc()
            pass

        #FILTER SOURCE IF POSSIBLE
        try:
            if  data['source'][0] != 'all':
                qParams.append('source:{}'.format(data['source'][0]))
        except KeyError:
            pass

        try:

            # PUTING THE PARAMS TOEGETHER
            qParams = [x for x in qParams if x]
            params["q"] = ' AND '.join(qParams)
            if journalFilter:
                params["fq"].append(journalFilter)

            if cType:
                params["fq"].append(cType)

            params["fq"].append(' AND '.join([x for x in [genes, mutations] if x]))

            # CT RETRIEVAL
            ctResults, ctFound = yield self.queryCTG(data, keywords, genes, mutations)
            
            # GET MEDLINE
            r = requests.post(self.reqPage, data=params)
            response = r.json()

            #===================================================================
            # WRITE QUERY TO DB
            #===================================================================

            try:
                queryID = data['queryID'][0]
            except KeyError:
                query = """INSERT INTO cancer.queries
                            (queries_userID, queries_genes, queries_mutations)
                            VALUES ('{}', '{}', '{}')""".format(userID, self.con.escape_string(genes),self.con.escape_string(mutations))
                queryID = yield self.executeQuery(query)

            query = """INSERT INTO  queriesResult
                      (queriesResult_userid,queriesResult_queryID,queriesResult_resultsPage,queriesResult_pmids)
                       VALUES ('{}','{}','{}','{}')""".format(userID,queryID,(int(data['currentPage'][0]) -1 ),",".join([x['pmid'] for x in response['response']['docs']]))
            res = yield self.executeQuery(query)

            if response['response']['numFound'] > 0:
                journals, sourceCount = yield self.getJournals(params, response['response']['numFound'])
                # if 'source' in data:
                #     sourceCount = [json.loads(item) for item in data['sourceCount'] ]

                if 'queryID' not in data:
                    maxTemp, minTemp = yield self.getYearsMinMax(params, response['response']['numFound'])
                    min=minTemp
                    max=maxTemp
                    minFilter=minTemp
                    maxFilter=maxTemp
                else:
                    min=int(data['minYear'][0])
                    max=int(data['maxYear'][0])
                    minFilter=int(data['minFiltered'][0])
                    maxFilter=int(data['maxFiltered'][0])

                resList ={
                        'docs':response['response']['docs'],
                        'ct':ctResults,
                        'numFound':sourceCount[0]['occurences'],
                        'numFoundCT':ctFound,
                        'journals': journals,
                        # 'sourceCount':sourceCount,
                        'queryID':queryID,
                        'minPublication':min,
                        'maxPublication':max,
                        'minPublicationFilter':minFilter,
                        'maxPublicationFilter':maxFilter
                    }

                # pp = pprint.PrettyPrinter(indent=4)
                # pp.pprint(resList['docs'])

        except Exception as e:
            print e
            traceback.print_exc()

        # print '************************************************'
        self.write(json.dumps(resList))

class CivicHandler(BaseHandler):
    #s@tornado.web.authenticated

    def get(self):

        data = self.request.arguments
        #print 'Recieved Data:\t', data
        civic = pd.read_csv(JOURNAL_CIVIC)
        #print civic

        try:
            civic = civic.head(int(data['TopCivic'][0]))
        except ValueError:
            pass

        #journals = [x[0] for x in civic.itertuples(False)]
        journals = [{'name':x[0], 'titleName': x[0] +' (' + str(x[1]) + ')', 'countValue':x[1]} for x in civic.itertuples(False)]
        #print journals
        self.write(json.dumps(journals))

class FeedbackHandler(MainHandler):
    #@tornado.web.authenticated
    def get(self):
        resList= {'success':'false'}
        # print 'current user\t'+tornado.escape.xhtml_escape(self.current_user)
        userId = tornado.escape.xhtml_escape(self.current_user)

        try:

            data = self.request.arguments
            # print 'Feedback params GET:\t',data
            res = []

            for k,v in data.iteritems():
                res.extend([ast.literal_eval(item) for item in v])

            feedbackItems = []
            for item in res:
                #feedbackItems.append((v for k,v in item.iteritems()))
                feedbackItems.append((userId,item["useful"],item['classification'],item['queryId'], item["pmid"]))

            # print feedbackItems

            con = mdb.connect('localhost', USER, PASS, db, use_unicode=True, charset='utf8')
            cur = con.cursor(mdb.cursors.DictCursor)

            cur.executemany("""INSERT INTO cancer.feedback 
                                (feedback_user,feedback_relevant,feedback_correctClassification,feedback_queryId,feedback_pmid) 
                                VALUES (%s,%s,%s,%s,%s)""", feedbackItems)
            con.commit()
            con.close()
            resList= {'success':'true'}

        except Exception as e:
            print e
            traceback.print_exc()

        self.write(json.dumps(resList))

class JournalsHandler(MainHandler):
    def get(self):
        resList = {}
        try:
            con = mdb.connect('localhost', USER, PASS, db, use_unicode=True, charset='utf8')
            cur = con.cursor(mdb.cursors.DictCursor)

            cur.execute("select journals_NlmId, journals_full from cancer.journals;")
            resList= cur.fetchall()
            con.commit()
            con.close()

        except Exception as e:
            print e

        #print resList
        self.write(json.dumps(resList))

class exampleQueriesHandler(BaseHandler):
    def get(self):
        resList = {}
        try:
            con = mdb.connect('localhost', USER, PASS, dbEval, use_unicode=True, charset='utf8')
            cur = con.cursor(mdb.cursors.DictCursor)

            cur.execute(
                "select * from evaluationQueries "
            )
            resList = cur.fetchall()
            con.commit()
            con.close()

        except Exception as e:
            print e

        # print json.dumps(resList)
        self.write(json.dumps(resList))
